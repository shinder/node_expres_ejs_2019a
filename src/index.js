var express = require('express');

var app = express();

app.use(express.static('public'));
// app.use(express.static(__dirname + '/../public'));

app.set('view engine', 'ejs');
// app.set('views', __dirname + '/../views');

// 引入套件
const bodyParser = require('body-parser');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

const session = require('express-session');
app.use(session({
    // 新用戶沒有使用到 session 物件時不會建立 session 和發送 cookie
    saveUninitialized: false,
    resave: false, // 沒變更內容是否強制回存
    secret: '加密用的字串',
    cookie: {
        maxAge: 1200000, // 20分鐘，單位毫秒
    }
}));

const cors = require('cors');
var whitelist = ['http://localhost:8080', undefined, 'http://localhost:3000'];
var corsOptions = {
    credentials: true,
    origin: function (origin, callback) {
        console.log('origin: '+origin);
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};
app.use(cors(corsOptions));
// app.use(cors());


app.get('/', function (request, response) {
    response.render('home', {name: 'Shinder'});
});

// src/index.js 加入路由
app.get('/sales', function (request, response) {
    const data = require(__dirname + '/../data/sales');
    response.render('sales', {
        sales: data
    });
});

// src/index.js 加入路由
// 引入核心套件 url
const url = require('url');
app.get('/try-querystring', (req, res) => {
    const urlParts = url.parse(req.url, true);
    console.log(urlParts); // 在 server 端查看
    res.render('try-querystring', {
        urlParts: urlParts
    });
});


// 取得 urlencoded parser, 不使用 qs lib, 而使用內建的 querystring lib
const urlencodedParser = bodyParser.urlencoded({extended: false});
app.get('/try-post-form', (req, res) => {
    res.render('try-post-form');
});
// 把 urlencodedParser 當 middleware
app.post('/try-post-form', (req, res) => {
    res.render('try-post-form', req.body);
});

const multer = require('multer');
const upload = multer({dest:'tmp_uploads/'});  // 設定上傳暫存目錄
const fs = require('fs'); // 處理檔案的核心套件
app.get('/try-upload', (req, res)=>{
    res.render('try-upload');
});
//
app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    console.log(req.file); // 查看裡面的屬性
    if(req.file && req.file.originalname){
        // 判斷是否為圖檔
        if(/\.(jpg|jpeg|png)$/i.test(req.file.originalname)) {
            fs.createReadStream(req.file.path)
                .pipe(
                    fs.createWriteStream('./public/img/' + req.file.originalname)
                );
        }
    }
    res.render('try-upload', {
        result: true,
        name: req.body.name,
        avatar: '/img/' + req.file.originalname
    });
});

app.get('/my-params1/:action/:id', (req, res)=>{
    res.json(req.params);
});

app.get('/my-params2/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.get('/my-params3/*/*?', (req, res)=>{
    res.json(req.params);
});

// 使用 regular expression 設定路由
app.get(/^\/hi\/?/, (req, res)=>{
    let result = {
        url : req.url
    };
    result.split = req.url.split('/');
    res.json(result);
});
// 手機號碼
app.get(/^\/09\d{2}\-?\d{3}\-?\d{3}/, (req, res)=>{
    let str = req.url.slice(1);
    str = str.split('-').join('');
    res.send('手機: ' + str);
});

// 在 src/index.js 內加入
const admin1 = require(__dirname + '/admins/admin1');
admin1(app);

// 在 src/index.js 內加入
const admin2Router = require(__dirname + '/admins/admin2');
app.use(admin2Router); //當成 middleware 使用

// 在 src/index.js 內加入
const admin3Router = require(__dirname + '/admins/admin3');
app.use('/admin3', admin3Router); // module 裡面的路徑用相對路徑

app.get('/try-session', (req, res)=>{
    req.session.views = req.session.views || 0; // 預設為 0
    req.session.views++;
    res.contentType('text/plain');
    res.write('拜訪次數:' + req.session.views + "\n");
    res.end( JSON.stringify(req.session));
});

app.post('/try-session2', (req, res)=>{
    req.session.views = req.session.views || 0;
    req.session.views ++;
    res.json({
        views: req.session.views
    })
});

app.get('/login', (req, res)=>{
    const data = {};
    if(req.session.flashMsg) { // 快閃訊息
        data.flashMsg = req.session.flashMsg;
        delete req.session.flashMsg;
    }
    data.isLogined = !! req.session.loginUser;
    data.loginUser = req.session.loginUser;
    res.render('login', data);
});
app.post('/login', (req, res)=>{
    if(req.body.user==='shin' && req.body.password==='1234'){
        req.session.loginUser = 'shin';
    } else {
        req.session.flashMsg = '帳號或密碼錯誤'; // 快閃訊息
    }
    res.redirect('/login');
});

app.get('/logout', (req, res)=>{
    delete req.session.loginUser;
    res.redirect('/login');
});


const moment = require('moment-timezone');
app.get('/try-moment', (req, res)=>{
    const myFormat = 'YYYY-MM-DD HH:mm:ss';
    const exp = req.session.cookie.expires;
    const mo1 = moment(exp);
    const mo2 = moment(new Date());
    res.contentType('text/plain');
    res.write( exp + "\n");
    res.write('台北 ' + mo1.format(myFormat) + "\n"); // 系統時區
    res.write('倫敦 ' + mo1.tz('Europe/London').format(myFormat) + "\n");
    res.write('台北 ' + mo2.format(myFormat) + "\n");
    res.write('倫敦 ' + mo2.tz('Europe/London').format(myFormat) + "\n");
    res.end( JSON.stringify(req.session));
});


// src/index.js 加入
var mysql = require('mysql');
var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'test'
});
db.connect(); // 連線
app.get('/sales3', (req, res)=>{
    var sql = "SELECT * FROM `sales`";
    db.query(sql, (error, results, fields)=>{
        if(error) throw error;
        console.log(results, fields);
        for(let v of results){
            v.birthday2 = moment(v.birthday).format('YYYY-MM-DD');
        }
        res.render('sales3', {
            sales: results
        });
    });
});

// src/index.js 內容
app.get('/sales3/add', (req, res)=>{
    res.render('sales3-add');
});
app.post('/sales3/add', (req, res)=>{
    const data = { success: false, message: {type: 'danger', text:''} };
    data.body = req.body; // 取得所有 post 資料
    if(!req.body.sales_id || !req.body.name || !req.body.birthday){
        data.message.text = '資料不齊全';
        return res.render('sales3-add', data);
    }
    // 內容見下一頁
    let sql = "INSERT INTO sales(sales_id, `name`, birthday) VALUES (?, ?, ?);";
    db.query( sql,
        [req.body.sales_id, req.body.name, req.body.birthday],
        (error, results, fields)=>{
            if(error) throw error;
            console.log(results);
            if(results.affectedRows===1){
                data.success = true;
                data.message.type = 'primary';
                data.message.text = '新增完成';
            } else {
                data.message.text = '資料沒有新增';
            }
            res.render('sales3-add', data);
        });
});

// src/index.js 新增內容
var bluebird = require('bluebird');
bluebird.promisifyAll(db); // db 為 mysql 連線物件
app.get('/try-bluebird', (req, res)=>{
    const output = [];
    db.queryAsync("SELECT * FROM sales WHERE sid=?", [3])
        .then( results=> {
            output.push(results[0]);
            return db.queryAsync("SELECT * FROM sales WHERE sid=?", [2]);
        })
        .then(results=>{
            output.push(results[0]);
            res.json(output);
        })
        .catch((error)=>{
            console.log('*** sql error ***: ', error);
        });
});

// --------------------------------------------
app.use((req, res) => {
    res.type('text/plain');
    res.status(404);
    res.send('404 - 找不到網頁');
});

app.listen(3000, function () {
    console.log('啟動 server 偵聽埠號 3000');
});
